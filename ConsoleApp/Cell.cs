﻿/*
MIT License

Copyright(c) 2018 Hammond Soares

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleApp
{
    /// <summary>
    /// Class the represents a cell.
    /// </summary>
    public class Cell
    {
        #region Fields

        /// <summary>
        /// The list of cell's neighbours.
        /// </summary>
        private List<Cell> _neighbours;

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="Cell"/> class.
        /// </summary>
        public Cell()
        {
            _neighbours = new List<Cell>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Cell"/> class.
        /// </summary>
        /// <param name="position">The position.</param>
        public Cell(Tuple<int, int> position)
            : this()
        {
            Position = position;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets a value indicating whether this cell is alive.
        /// </summary>
        public bool IsAlive { get; set; }

        /// <summary>
        /// Gets the cell position.
        /// </summary>
        public Tuple<int, int> Position { get; private set; }

        /// <summary>
        /// Gets the number of neighbours alive.
        /// </summary>
        private int NeighboursAlive
        {
            get
            {
                return _neighbours.Where(n => n.IsAlive).Count();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds the neighbours.
        /// </summary>
        /// <param name="cell">The neighbour cell.</param>
        public void AddNeighbours(Cell cell)
        {
            _neighbours.Add(cell);
        }

        /// <summary>
        /// Checks if the neighbours are alive or dead.
        /// </summary>
        public void CheckNeighbours()
        {
            bool shouldBeBorn = (!IsAlive && NeighboursAlive == 3);
            bool shouldSurvive = (IsAlive && NeighboursAlive >= 2 && NeighboursAlive <= 3);
            IsAlive = (shouldBeBorn || shouldSurvive);
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            string retVal = "Cell";

            if (Position != null)
            {
                retVal = $"({Position.Item1}, {Position.Item2})";
            }

            return retVal;
        }

        #endregion
    }
}