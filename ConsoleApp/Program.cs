﻿/*
MIT License

Copyright(c) 2018 Hammond Soares

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;

namespace ConsoleApp
{
    /// <summary>
    /// The main class.
    /// </summary>
    internal class Program
    {
        #region Constants

        /// <summary>
        /// The maximum number of lines on the screen.
        /// </summary>
        private const int MAX_LINES = 29;

        /// <summary>
        /// The maximum number of columns on the screen.
        /// </summary>
        private const int MAX_COLUMNS = 119;

        /// <summary>
        /// The cell grid.
        /// </summary>
        private static CellGrid _cellGrid = new CellGrid(MAX_LINES, MAX_COLUMNS);

        #endregion Constants

        /// <summary>
        /// The program entry point.
        /// </summary>
        /// <param name="args">The arguments.</param>
        private static void Main(string[] args)
        {
            Console.WindowHeight = MAX_LINES + 1; // Prevent horizontal scroll
            Console.WindowWidth = MAX_COLUMNS;

            while (true)
            {
                _cellGrid.ProcessCells();
                PrintCellsGrid();
            }
        }

        /// <summary>
        /// Prints the cells on the grid.
        /// </summary>
        private static void PrintCellsGrid()
        {
            Console.SetCursorPosition(0, 0);
            Console.CursorVisible = false;

            _cellGrid.Print(Console.Write, Console.WriteLine);
        }
    }
}