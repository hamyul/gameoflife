﻿/*
MIT License

Copyright(c) 2018 Hammond Soares

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using System.Drawing;

namespace ConsoleApp
{
    /// <summary>
    /// Class that represents a cell grid.
    /// </summary>
    public class CellGrid
    {
        /// <summary>
        /// The maximum number of lines
        /// </summary>
        private int _maxLines;

        /// <summary>
        /// The maximum number of columns
        /// </summary>
        private int _maxColumns;

        /// <summary>
        /// The cells storage.
        /// </summary>
        private Cell[,] _cells;

        /// <summary>
        /// Initializes a new instance of the <see cref="CellGrid"/> class.
        /// </summary>
        /// <param name="maxLines">The maximum number of lines.</param>
        /// <param name="maxColumns">The maximum number of columns.</param>
        public CellGrid(int maxLines, int maxColumns)
        {
            _cells = new Cell[maxLines, maxColumns];
            _maxLines = maxLines;
            _maxColumns = maxColumns;

            CreateCellsGrid();
        }

        /// <summary>
        /// Gets the cell on the informed position.
        /// </summary>
        /// <param name="line">The line.</param>
        /// <param name="column">The column.</param>
        /// <returns>The cell found.</returns>
        public Cell GetCell(int line, int column)
        {
            return GetCell(new Point(line, column));
        }

        /// <summary>
        /// Gets the cell on the informed position.
        /// </summary>
        /// <param name="point">The position on the grid.</param>
        /// <returns>The cell found.</returns>
        protected Cell GetCell(Point point)
        {
            return _cells[point.X, point.Y];
        }

        /// <summary>
        /// Prints the cells on the grid.
        /// </summary>
        public void Print(Action<string> print, Action<string> printLine)
        {
            for (int line = 0; line < _maxLines; line++)
            {
                for (int column = 0; column < _maxColumns; column++)
                {
                    if (GetCell(line, column).IsAlive)
                    {
                        print("@");
                    }
                    else
                    {
                        print(" ");
                    }
                }
                printLine(string.Empty);
            }
        }

        /// <summary>
        /// Makes every cell check for its neighbours living state.
        /// </summary>
        public void ProcessCells()
        {
            for (int line = 0; line < _maxLines; line++)
            {
                for (int column = 0; column < _maxColumns; column++)
                {
                    GetCell(line, column).CheckNeighbours();
                }
            }
        }

        /// <summary>
        /// Creates the cells.
        /// </summary>
        private void CreateCellsGrid()
        {
            Random rnd = new Random(0);

            for (int line = 0; line < _maxLines; line++)
            {
                for (int column = 0; column < _maxColumns; column++)
                {
                    _cells[line, column] = new Cell(new Tuple<int, int>(line, column));
                    var value = rnd.Next(100);
                    _cells[line, column].IsAlive = value >= 90;
                }
            }

            AddNeighboursForEachCell();
        }

        /// <summary>
        /// Adds the neighbours.
        /// </summary>
        private void AddNeighboursForEachCell()
        {
            for (int line = 0; line < _maxLines; line++)
            {
                for (int column = 0; column < _maxColumns; column++)
                {
                    // Left
                    if (column > 0)
                    {
                        _cells[line, column].AddNeighbours(_cells[line, column - 1]);
                    }
                    //Right
                    if (column < _maxColumns - 1)
                    {
                        _cells[line, column].AddNeighbours(_cells[line, column + 1]);
                    }
                    // Top
                    if (line > 0)
                    {
                        _cells[line, column].AddNeighbours(_cells[line - 1, column]);
                    }
                    // Bottom
                    if (line < _maxLines - 1)
                    {
                        _cells[line, column].AddNeighbours(_cells[line + 1, column]);
                    }
                    // Top-Left
                    if (line > 0 && column > 0)
                    {
                        _cells[line, column].AddNeighbours(_cells[line - 1, column - 1]);
                    }
                    // Top-Right
                    if (line > 0 && column < _maxColumns - 1)
                    {
                        _cells[line, column].AddNeighbours(_cells[line - 1, column + 1]);
                    }
                    // Bottom-Left
                    if (line < _maxLines - 1 && column > 0)
                    {
                        _cells[line, column].AddNeighbours(_cells[line + 1, column - 1]);
                    }
                    // Bottom-Right
                    if (line < _maxLines - 1 && column < _maxColumns - 1)
                    {
                        _cells[line, column].AddNeighbours(_cells[line + 1, column + 1]);
                    }
                }
            }
        }
    }
}