/*
MIT License

Copyright(c) 2018 Hammond Soares

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ConsoleApp.Tests
{
    /*
    RULES:

    a) Birth: if exactly three of its neighbours are alive, the cell will become alive at the next step.
    b) Survival: if the cell has two or three live neighbours, the cell remains alive. Otherwise, the cell will die.
    c) Death by loneliness: if the cell has only zero or one live neighbours, the cell will become dead at the next step.
    d) if the cell alive and has more than three live neighbours, the cell also dies (Death by overcrowding).
    */

    [TestClass]
    public class CellTest
    {
        #region Constants

        /// <summary>
        /// The maximum number of neighbours.
        /// </summary>
        private const int MAX_NEIGHBOURS = 8;

        #endregion

        #region Dead Cell

        [TestMethod]
        public void should_not_be_born_when_no_neighbours_alive()
        {
            var cell = CreateCell(false, 0);
            cell.CheckNeighbours();

            Assert.IsFalse(cell.IsAlive);
        }

        [TestMethod]
        public void should_not_be_born_when_one_neighbours_alive()
        {
            var cell = CreateCell(false, 1);
            cell.CheckNeighbours();

            Assert.IsFalse(cell.IsAlive);
        }

        [TestMethod]
        public void should_not_be_born_when_two_neighbours_alive()
        {
            var cell = CreateCell(false, 2);
            cell.CheckNeighbours();

            Assert.IsFalse(cell.IsAlive);
        }

        [TestMethod]
        public void should_born_when_three_neighbours_alive()
        {
            var cell = CreateCell(false, 3);
            cell.CheckNeighbours();

            Assert.IsTrue(cell.IsAlive);
        }

        [TestMethod]
        public void should_not_be_born_when_four_neighbours_alive()
        {
            var cell = CreateCell(false, 4);
            cell.CheckNeighbours();

            Assert.IsFalse(cell.IsAlive);
        }

        [TestMethod]
        public void should_not_be_born_when_five_neighbours_alive()
        {
            var cell = CreateCell(false, 5);
            cell.CheckNeighbours();

            Assert.IsFalse(cell.IsAlive);
        }

        [TestMethod]
        public void should_not_be_born_when_six_neighbours_alive()
        {
            var cell = CreateCell(false, 6);
            cell.CheckNeighbours();

            Assert.IsFalse(cell.IsAlive);
        }

        [TestMethod]
        public void should_not_be_born_when_seven_neighbours_alive()
        {
            var cell = CreateCell(false, 7);
            cell.CheckNeighbours();

            Assert.IsFalse(cell.IsAlive);
        }

        [TestMethod]
        public void should_not_be_born_when_eight_neighbours_alive()
        {
            var cell = CreateCell(false, 8);
            cell.CheckNeighbours();

            Assert.IsFalse(cell.IsAlive);
        }

        #endregion Dead Cell

        #region Live Cell

        [TestMethod]
        public void should_survive_when_two_neighbours_alive()
        {
            var cell = CreateCell(true, 2, 6);
            cell.CheckNeighbours();

            Assert.IsTrue(cell.IsAlive);
        }

        [TestMethod]
        public void should_survive_when_more_than_two_neighbours_alive()
        {
            var cell = CreateCell(true, 3, 5);
            cell.CheckNeighbours();

            Assert.IsTrue(cell.IsAlive);
        }

        [TestMethod]
        public void should_die_when_it_has_one_neighbour_alive()
        {
            var cell = CreateCell(true, 1, 7);
            cell.CheckNeighbours();

            Assert.IsFalse(cell.IsAlive);
        }

        [TestMethod]
        public void should_die_when_it_has_no_neighbours_alive()
        {
            var cell = CreateCell(true, 0, 8);
            cell.CheckNeighbours();

            Assert.IsFalse(cell.IsAlive);
        }

        [TestMethod]
        public void should_die_when_it_has_four_neighbours_alive()
        {
            var cell = CreateCell(true, 4, 4);
            cell.CheckNeighbours();

            Assert.IsFalse(cell.IsAlive);
        }

        [TestMethod]
        public void should_die_when_it_has_more_than_four_neighbours_alive()
        {
            var cell = CreateCell(true, 7, 1);
            cell.CheckNeighbours();

            Assert.IsFalse(cell.IsAlive);
        }

        #endregion Live Cell

        #region Private Methods

        private Cell CreateCell(bool isAlive, int liveNeighbours)
        {
            return CreateCell(isAlive, liveNeighbours, MAX_NEIGHBOURS - liveNeighbours);
        }

        private Cell CreateCell(bool isAlive, int liveNeighbours, int deadNeighbours)
        {
            if (liveNeighbours + deadNeighbours > 8)
                throw new ArgumentOutOfRangeException();

            if (liveNeighbours + deadNeighbours == 0)
                throw new ArgumentOutOfRangeException();

            if (liveNeighbours > 8 || deadNeighbours > 8)
                throw new ArgumentOutOfRangeException();

            var cell = new Cell() { IsAlive = isAlive };

            // Create live neighbours
            for (int i = 0; i < liveNeighbours; i++)
            {
                cell.AddNeighbours(new Cell() { IsAlive = true });
            }

            // Create dead neighbours
            for (int i = 0; i < deadNeighbours; i++)
            {
                cell.AddNeighbours(new Cell());
            }

            return cell;
        }

        #endregion Private Methods
    }
}